/*
 * string.h
 *
 *  Created on: Jan 3, 2018
 *      Author: seeseemelk
 */

#ifndef STRING_H_
#define STRING_H_

#include <stddef.h>

void* memcpy(void* dest, void* src, size_t num);
void* memset(void* ptr, int value, size_t num);

#endif /* STRING_H_ */
