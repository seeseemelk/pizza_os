/*
 * math.h
 *
 *  Created on: Jan 7, 2018
 *      Author: seeseemelk
 */

#ifndef MATH_H_
#define MATH_H_
#include <stddef.h>

size_t ceilg(size_t n, size_t granularity);
size_t ceildiv(size_t a, size_t b);

#endif /* MATH_H_ */
